﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AzureAutomation.Models
{
    public static class ConfigurationApp
    {
        public static string ClientId { get; set; }
        public static string ClientSecret { get; set; }
        public static string AadInstance { get; set; }
        public static string Tenant { get; set; }
        public static string RedirectUrl { get; set; }
        public static string VHDURILinux { get; set; }
        public static string VHDURIWindow { get; set; }
        //public static List<string> VHDList { get; set; }
        public static string Authority { get; set; }
        public static string Connection { get; set; }
        public static string Access_token { get; set; }
    }
}