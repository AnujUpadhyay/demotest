﻿using AzureAutomation.Models;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace webhookexample.Models
{
    public static class Commonfuntion
    {
        //This method responsible  for insertion record in DB
        public static void InsertalertData(Alertstatus alertstatus)
        {
            try
            {

                using (var sqlConnection = new SqlConnection(ConfigurationApp.Connection))
                {
                    sqlConnection.Open();
                    var cmd =
                        new SqlCommand("USP_Alert", connection: sqlConnection)
                        {
                            CommandType = CommandType.StoredProcedure
                        };
                    cmd.Parameters.AddWithValue("@Operation", "Insert");
                    cmd.Parameters.AddWithValue("@ResourceName", alertstatus.ResourceName);
                    cmd.Parameters.AddWithValue("@Status", alertstatus.Status);
                    cmd.Parameters.AddWithValue("@Timestamp", DateTime.Now);
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static DataTable getResourcenameDB()
        {
            DataTable _dtgetresourcename = new DataTable();
            try
            {

                using (var sqlConnection = new SqlConnection(ConfigurationApp.Connection))
                {
                    sqlConnection.Open();
                    var cmd =
                        new SqlCommand("USP_AlertGET", connection: sqlConnection)
                        {
                            CommandType = CommandType.StoredProcedure
                        };
                    SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(cmd);
                    sqlDataAdapter.Fill(_dtgetresourcename);

                    return _dtgetresourcename;

                }
            }
            catch (Exception)
            {
                throw;
            }
        }


        /// <summary>
        /// Get Authentication token
        /// </summary>
        /// <returns>return string as a token</returns>
        public static string GetToken()
        {

            var clientCredential = new ClientCredential(ConfigurationApp.ClientId, ConfigurationApp.ClientSecret);
            var context = new AuthenticationContext(string.Format("https://login.microsoftonline.com/{0}", ConfigurationApp.Tenant));
            var result = context.AcquireTokenAsync("https://management.azure.com/", clientCredential);
            if (result == null)
            {
                throw new InvalidOperationException("Failed to obtain the JWT token");
            }

            string token = result.Result.AccessToken;
            return token;

        }



        /// <summary>
        /// Call Azure Rest API 
        /// </summary>
        /// <param name="URI">Rest API URI</param>
        /// <param name="body">pass parameter in body</param>
        /// <param name="token">Authrization token</param>
        /// <param name="Method">Name of method like:PUT,DELETE</param>
        public static async Task<HttpResponseMessage> CallAzureRestAPI(string URI, string body, HttpMethod Method)
        {
            HttpResponseMessage HttpResponseMessage = null;
            try
            {

                Uri uri = new Uri(String.Format(URI));
                HttpClient _client = new HttpClient();
                _client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", GetToken().ToString());
                HttpRequestMessage request = new HttpRequestMessage
                {
                    // Content = new StringContent(body, Encoding.UTF8, "application/json"),
                    Method = Method,
                    RequestUri = new Uri(URI)
                };
                if (Method != HttpMethod.Get)
                {
                    request.Content = new StringContent(body, Encoding.UTF8, "application/json");
                }

                HttpResponseMessage = await _client.SendAsync(request);


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return HttpResponseMessage;

        }

        /// <summary>
        /// Check Resource exist 
        /// </summary>
        /// <param name="_mURI"></param>
        /// <returns></returns>
        public static async Task<Alertstatus> fetchrestapidata(string _mURI)
        {
            string message = string.Empty;
            HttpResponseMessage _httpResponseMessage = null;
            Alertstatus alertstatus = new Alertstatus();
            //  List<string> data = null;
            try
            {
                string URI = _mURI;

                _httpResponseMessage = await CallAzureRestAPI(URI, string.Empty, HttpMethod.Get);
                var content = await _httpResponseMessage.Content.ReadAsStringAsync();
                if (content != null)
                {

                    JObject obj = JObject.Parse(content);
                    var alertstatuss =
                              from p in obj["value"]
                              select p;
                    foreach (var item in alertstatuss)
                    {
                        alertstatus.Status = item["properties"]["status"].ToString();
                        string timestamp = item["properties"]["timestamp"].ToString();
                    }

                  
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return alertstatus;
        }
    }

    public static class CustomResponse
    {
        public static HttpResponseMessage ClientResponseMessage(string Message)
        {
            HttpResponseMessage _httpResponseMessage = new HttpResponseMessage();
            StringContent stringContent = new StringContent(Message);
            _httpResponseMessage.Content = stringContent;
            return _httpResponseMessage;

        }
        public static HttpResponseMessage Response(string type, string name, string status)
        {
            HttpResponseMessage _httpResponseMessage;
            string ResponseJSON = "{ 'Resource Type':'" + type + "'" + ",'Resource Name':'" + name + "'" + ",'Status':'" + status + "'" + "}";
            JObject rsponse = JObject.Parse(ResponseJSON.ToString());
            _httpResponseMessage = CustomResponse.ClientResponseMessage(rsponse.ToString());
            return _httpResponseMessage;
        }
       


    }
}