﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webhookexample.Models
{
    public class Alertstatus
    {
        public string ResourceName { get; set; }
        public string Status { get; set; }
    }
}