﻿using AzureAutomation.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace webhookexample
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            ConfigurationApp.ClientId = "aaf343c6-1650-48aa-8869-7d092c22c5cf";
            ConfigurationApp.ClientSecret = "WLfhupKndf4bKEF/dpPywQmh3UBLqqLOTdW94JFFY70=";
            ConfigurationApp.Tenant = "ee2e3eeb-2625-40ae-8d79-2d3d47717f23";
            ConfigurationApp.Connection = ConfigurationManager.ConnectionStrings["AzureSQLDB"].ConnectionString; 
        }
    }
}
