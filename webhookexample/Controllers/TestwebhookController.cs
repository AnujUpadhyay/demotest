﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using webhookexample.Models;

namespace webhookexample.Controllers
{
    public class TestwebhookController : ApiController
    {
        [System.Web.Http.Route("~/api/testwebhook")]
        [System.Web.Http.HttpPost]
        public void testwebhookAPI1([FromUri] string Resourcename, [FromUri] string Status)
        {
            Alertstatus alertstatus = new Alertstatus();
            alertstatus.ResourceName = Resourcename;
            alertstatus.Status = Status;

            Commonfuntion.InsertalertData(alertstatus);
        }

        [System.Web.Http.Route("~/api/test")]
        [System.Web.Http.HttpPost]
        public async Task<HttpResponseMessage> CallRestapiforalertstatus([FromUri] string Resourcename, [FromUri] string alertname)
        {
            HttpResponseMessage mHttpResponseMessage = null;
            //string alertname = "PSJH%20CPU";
            string URI = "https://management.azure.com/subscriptions/ee72cb97-e3ac-47b3-b068-d9467f338fbb/resourceGroups/psjh/providers/Microsoft.Insights/metricAlerts/" + alertname + "/status?api-version=2018-03-01";
            Alertstatus alertstatus = await Commonfuntion.fetchrestapidata(URI);
            alertstatus.ResourceName = Resourcename;

            Commonfuntion.InsertalertData(alertstatus);
            return mHttpResponseMessage = CustomResponse.ClientResponseMessage("Record stored");
        }
        [System.Web.Http.Route("~/api/getresource")]
        [System.Web.Http.HttpGet]
        public void GetresourcesAPI2()
        {
            Commonfuntion.getResourcenameDB();
        }
    }
}
